artifactory_url="http://10.19.41.1:8081/artifactory"

repo="libs-snapshot-local"
artifacts="com/amdocs/StaticWebCode"           

url=$artifactory_url/$repo/$artifacts

file=`curl -s $url/maven-metadata.xml`

version=`curl -s $url/maven-metadata.xml | grep latest | sed "s/.*<latest>\([^<]*\)<\/latest>.*/\1/"`

build=`curl -s $url/$version/maven-metadata.xml | grep '<value>' |head -1 | sed "s/.*<value>\([^<]*\)<\/value>.*/\1/"`


echo "Build = " + $build + " Version = " + $version

BUILD_LATEST="$url/$version/StaticWebCode-$build.war"
echo "File Name  = " + $BUILD_LATEST

#HelloWeb-2.0-20210901.114110-2.war

echo $BUILD_LATEST > filename.txt
